import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiurl=" http://localhost:3000/posts"
  carturl="http://localhost:3000/carts"
  constructor(private http:HttpClient) 
  { 

  }
  post(data):Observable<any>
  {
    return this.http.post<any>(this.apiurl,data);
  }
  get():Observable<any>
  {
    return this.http.get<any>(this.apiurl);
  }
  postcart(data):Observable<any>
  {
    return this.http.post<any>(this.carturl,data);
  }
  getcart():Observable<any>
  {
    return this.http.get<any>(this.carturl);
  }
  deletecart(id):Observable<any>
  {
    return this.http.delete<any>(this.carturl,id)
  }

}
