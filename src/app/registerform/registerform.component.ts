import { Component, OnInit } from '@angular/core';
import{FormGroup,FormBuilder,Validators,FormControl, AbstractControl, NgModel} from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import {DatePipe} from '../date.pipe'


@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.css']
})
export class RegisterformComponent implements OnInit {
  registerForm: FormGroup;
 nowAge;
 final: any;
 date= new Date().getFullYear();
 birthdate:any;
 
  constructor(private bulid:FormBuilder,private route:Router) 
  {
   
    this.registerForm=this.bulid.group
    ({
      firstName:['',Validators.required],
      lastName:['',Validators.required],
      middleName:['',Validators.required],
      dateOfBirth:['',Validators.required],
      age:[''],
      mobile:['',[Validators.required,Validators.maxLength(10),Validators.pattern('^[987]+.*$')]],
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required,Validators.minLength(8),Validators.pattern('^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()]).*$')]],
      confirmPassword:['',Validators.required],
    },
    {
        validator: ConfirmedValidator('password','confirmPassword')
    },
   
      )
      // const age=this.registerForm.get('dateOfBirth').value
      // this.final=date.transform(age);
      
    
   }
   

  ngOnInit(): void {
    

  }
 
handleSubmit()
{
    const key="userdetails";
     console.log(this.registerForm.value);
     localStorage.setItem(key, JSON.stringify(this.registerForm.value));
     this.route.navigate(["/userdetails"]);
}
ageCalculation()
     {
         if(this.birthdate=this.registerForm.value.dateOfBirth)
          {
            const convertAge=new Date(this.birthdate);
            var timeDiff = Math.abs(Date.now() - convertAge.getTime());
           this.nowAge=Math.floor((timeDiff / (1000 * 3600 * 24))/365);
           console.log(this.nowAge);
           this.registerForm.value.age=this.nowAge;
           return this.nowAge;
        }
      }

}
function ConfirmedValidator(controlName: string, matchingControlName: string){
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
       if (matchingControl.errors &&  !matchingControl.errors.ConfirmedValidator) {
          return;
      }
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ ConfirmedValidator: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}


