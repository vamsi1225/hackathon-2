import { Directive,ElementRef,HostListener } from '@angular/core';

@Directive({
  selector: '[appPassword]'
})
export class PasswordDirective {

  constructor(private element:ElementRef) { }


private color(color:string)
{
  this.element.nativeElement.style.backgroundColor=color;
}
private size(size: string)
{
  this.element.nativeElement.style.fontSize=size;
}
@HostListener('mouseenter')onMouseEnter()
{
  this.color("blue");
  this.size("20px");
}
@HostListener('mouseleave') onMouseLeave()
{
  this.color(null);
  this.size(null);
}
}
