import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  
  details:any[];
  cart:any[];
  count:number=0;
  filterTerm: String;
  badgeCounter: number;
  key="count";
  key1="badgeCounter";
  
  
 
  // button array to use in html (only static 9 products)
  button:any=[{
    btnValue:true,
    id:1,
    bcount:0
  },
  {
    btnValue:true,
    id:2,
    bcount:0
  },
  {
    btnValue:true,
    id:3,
    bcount:0
  },
  {
    btnValue:true,
    id:4,
    bcount:0
  },
  {
    btnValue:true,
    id:5,
    bcount:0
  },
  {
    btnValue:true,
    id:6,
    bcount:0
  },
  {
    btnValue:true,
    id:7,
    bcount:0
  },
  {
    btnValue:true,
    id:8,
    bcount:0
  },
  {
    btnValue:true,
    id:9,
    bcount:0
  },
 
];


  constructor(private service:ProductService) { 
    this.badgeCounter=0;
  }
  
  // all the get methods from the service class
  ngOnInit(): void {
    this.service.get().subscribe((data)=>{console.log(this.details=data)});
    this.service.getcart().subscribe((data)=>{console.log(this.cart=data)});
    localStorage.setItem('count',JSON.stringify(0));
    this.count=JSON.parse(localStorage.getItem('count'));
  }
  // to enable the add button
  enablebtn(id)
{
  for(const b of this.button)
  {
    if(b.id==id)
    {
      b.btnValue=false;
    }
  }
  
}
// function to add the products
addProduct(id)
{ 
  if(this.count<5 && this.button[id-1].bcount<5 )
  {
   this.button[id-1].bcount=this.button[id-1].bcount+1; 
   const updateserver:any=
     {
       productname: this.details[id-1].productname,
       imageurl:this.details[id-1].imageurl,
       productid: this.details[id-1].id
     } 
   this.service.postcart(updateserver).subscribe((data)=>{console.log(data)});
   this.count=this.count+1;
   localStorage.setItem(this.key,JSON.stringify(this.count));
  }
  else
  {
    localStorage.setItem(this.key,JSON.stringify(this.count));
  }
 increment(); // for notification number

 }
removeProduct(id)
{
  if((this.count>0 && this.count<=5)&&(this.button[id-1].bcount>0 && this.button[id-1].bcount<=5))
  {
    this.button[id-1].bcount=this.button[id-1].bcount-1;
    
    this.service.deletecart(id).subscribe((data)=>{console.log(data)});//not working properly
    
    this.count=this.count-1;
    localStorage.setItem(this.key,JSON.stringify(this.count));
  }
  decrement();// for notification number

}




}
// for notification number to change dynamically
function increment() {
  this.badgeCounter++;
  localStorage.setItem(this.key1,JSON.stringify(this.badgeCounter));
    
}
function decrement() 
{
  if(this.badgeCounter<0)
  return;
  this.badgeCounter++;
  localStorage.setItem(this.key1,JSON.stringify(this.badgeCounter));

}


