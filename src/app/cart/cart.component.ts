import { Component, OnInit } from '@angular/core';
import {ProductService} from '../product.service'
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  details:any;

  constructor(private service:ProductService) { }

  ngOnInit(): void {
    this.service.getcart().subscribe((data)=>{this.details=data})
  }

}
