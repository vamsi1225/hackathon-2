import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-productadd',
  templateUrl: './productadd.component.html',
  styleUrls: ['./productadd.component.css']
})
export class ProductaddComponent implements OnInit {
  productdetails={
    productname:'',
    imageurl:''
  }
 //injection of service class
  constructor(private service:ProductService) { }

  ngOnInit(): void {
   
  }
  // method to add into db.json
  addproduct()
  {
    this.service.post(this.productdetails).subscribe((data)=>{console.log(data)});
  }


}
