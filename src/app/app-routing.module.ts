import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { ContactusComponent } from './contactus/contactus.component';
import { HomeComponent } from './home/home.component';
import { ProductaddComponent } from './productadd/productadd.component';
import { ProductsComponent } from './products/products.component';
import { SuccessComponent } from './success/success.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';

const routes: Routes = [
                        {path:'',redirectTo:"/home",pathMatch:"full"},
                        {path:'home',component:HomeComponent},
                        {path:'userdetails',component:UserdetailsComponent},
                        {path:'productadd',component:ProductaddComponent},
                        {path:'products',component:ProductsComponent},
                        {path:'cart',component:CartComponent},
                        {path:'contactus',component:ContactusComponent},
                        {path:'success',component:SuccessComponent}
                      
                      ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
